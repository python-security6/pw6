import threading
import time


def eat_breakfast():
    time.sleep(2)
    print("You eat breakfast")


def drink_cofee():
    time.sleep(3)
    print("You drank cofee")


def study():
    time.sleep(4)
    print("You finish studying")


x= threading.Thread(target=eat_breakfast,args=())
x.start()


y= threading.Thread(target=drink_cofee,args=())
y.start()


print(threading.active_count())
print(threading.enumerate())

# It took less time to complete thank to multithreading
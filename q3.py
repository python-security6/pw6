import threading
import time


def factorial(n):
    if n < 1:
      return 1
    else:
        returnNumber = n * factorial( n - 1 )  # рекусрсивынй вызов
        return returnNumber


def fibonacci_print(n):
    print(factorial(n))


x= threading.Thread(target=fibonacci_print,args=(35,))
x.start()


y= threading.Thread(target=fibonacci_print,args=(40,))
y.start()


z= threading.Thread(target=fibonacci_print,args=(45,))
z.start()
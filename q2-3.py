from multiprocessing import Process, cpu_count
import time


def counter(num):
    count = 0
    while count < num:
        count += 1


def main():
    start = time.perf_counter()
    a = Process(target=counter,args= (45000000,))
    b = Process(target=counter,args= (45000000,))
    c = Process(target=counter,args= (30000000,))


    a.start()
    b.start()
    c.start()


    a.join()
    b.join()
    c.join()


    print("finished in:", time.perf_counter()-start, "seconds")


if __name__ == '__main__':
    main()
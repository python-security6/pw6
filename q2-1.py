import threading
import time


def eat_breakfast():
    time.sleep(2)
    print("You eat breakfast")


def drink_cofee():
    time.sleep(3)
    print("You drank cofee")


def study():
    time.sleep(4)
    print("You finished studying")


eat_breakfast()
drink_cofee()
study()


print("The number of threads: "+str(threading.active_count()))
print(threading.enumerate())

# it's all in 1 thread(main thread)